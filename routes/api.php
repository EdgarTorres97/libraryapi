<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\WriterController;
use App\Http\Controllers\LibraryController;
use App\Http\Controllers\EditorialController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/books', [BookController::class, 'index']);
Route::post('/books', [BookController::class, 'store']);
Route::get('/books/{id}', [BookController::class, 'show']);
Route::put('/books/{id}', [BookController::class, 'update']);
Route::delete('/books/{id}', [BookController::class, 'delete']);
Route::get('/books/{id}/restore', [BookController::class, 'restore']);
Route::get('/books/{id}/destroy', [BookController::class, 'destroy']);
Route::get('books/find/{word}', [BookController::class, 'find']);

Route::get('/writers/getall', [WriterController::class, 'getAll']);
Route::get('/libraries/getall', [LibraryController::class, 'getAll']);
Route::get('/editorials/getall', [EditorialController::class, 'getAll']);
