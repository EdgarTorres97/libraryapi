<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Writer;

class WriterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $this->insert(1,'Edgar Allan','Poe');
        $this->insert(2,'Jane','Austen');
        $this->insert(3,'Miguel de Cervantes','Saavedra');
        $this->insert(4,'Agatha','Chistie');
        $this->insert(5,'Paulo','Coelho');
    }

    private function insert($id,$first_name,$second_name){
        $writer = new Writer();
        $writer->id = $id;
        $writer->first_name = $first_name;
        $writer->second_name = $second_name;
        $writer->save();
    }
}
