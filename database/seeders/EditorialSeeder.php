<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Editorial;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $this->insert(1,'Alianza',1);
        $this->insert(2,'Ariel',2);
        $this->insert(3,'Cátedra',3);
        $this->insert(4,'Marcial Pons',4);
        $this->insert(5,'Tecnos',5);
    }

    private function insert($id,$name,$editorial_address_id){
        $editorial = new Editorial();
        $editorial->id = $id;
        $editorial->name = $name;
        $editorial->editorial_address_id = $editorial_address_id;
        $editorial->save();
    }
}
