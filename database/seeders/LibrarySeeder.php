<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Library;

class LibrarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $this->insert(1,'La Murciélaga',1);
        $this->insert(2,'El Desastre Café',2);
        $this->insert(3,'La increíble librería',3);
        $this->insert(4,'La Moraleja',4);
        $this->insert(5,'Casa Bosques',5);
    }

    private function insert($id,$name,$library_address_id){
        $library = new Library();
        $library->id = $id;
        $library->name = $name;
        $library->library_address_id = $library_address_id;
        $library->save();
    }
}
