<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\LibraryAddress;

class LibraryAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $this->insert(1,'77555','Calle 4');
        $this->insert(2,'77123','Calle 5');
        $this->insert(3,'77234','Calle 6');
        $this->insert(4,'77456','Calle 7');
        $this->insert(5,'77534','Calle 8');
    }

    private function insert($id,$postal_code,$street){
        $library_address = new LibraryAddress();
        $library_address->id = $id;
        $library_address->postal_code = $postal_code;
        $library_address->street = $street;
        $library_address->save();
    }
}
