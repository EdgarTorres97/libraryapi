<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\AddressEditorial;

class AddressEditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $this->insert(1,'77111','Calle 10');
        $this->insert(2,'77222','Calle 15');
        $this->insert(3,'77333','Calle 16');
        $this->insert(4,'77444','Calle 17');
        $this->insert(5,'77555','Calle 18');
    }

    private function insert($id,$postal_code,$street){
        $address_editorial = new AddressEditorial();
        $address_editorial->id = $id;
        $address_editorial->postal_code = $postal_code;
        $address_editorial->street = $street;
        $address_editorial->save();
    }
}
