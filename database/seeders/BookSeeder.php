<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $lorem = 'Lorem ipsum dolor sit amet consectetur adipisicing elit.';
        $this->insert(1,'Don Quijote de la mancha',$lorem,'español','200','2020-02-27','12341234',1,1,1);
        $this->insert(2,'Historia de dos ciudades',$lorem,'español','200','2019-01-30','12341234',2,2,2);
        $this->insert(3,'El señor de los anillos',$lorem,'español','200','2017-03-17','12341234',3,3,3);
        $this->insert(4,'El principito',$lorem,'español','200','2000-04-15','12341234',4,4,4);
        $this->insert(5,'El hobbit',$lorem,'español','200','1990-06-10','12341234',5,5,5);

    }

    private function insert(
        $id,
        $name,
        $synopsis,
        $language,
        $page_number,
        $publication_date,
        $isbn,
        $writer_id,
        $editorial_id,
        $library_id
    ){
        $book = new Book();
        $book->id = $id;
        $book->name = $name;
        $book->synopsis = $synopsis;
        $book->language = $language;
        $book->page_number = $page_number;
        $book->publication_date = $publication_date;
        $book->isbn = $isbn;
        $book->writer_id = $writer_id;
        $book->editorial_id = $editorial_id;
        $book->library_id = $library_id;
        $book->save();
    }


}
