<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([LibraryAddressSeeder::class]);
        $this->call([LibrarySeeder::class]);
        $this->call([EditorialAddressSeeder::class]);
        $this->call([EditorialSeeder::class]);
        $this->call([WriterSeeder::class]);
        $this->call([BookSeeder::class]);

    }
}
