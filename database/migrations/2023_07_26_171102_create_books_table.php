<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('synopsis');
            $table->string('language');
            $table->integer('page_number');
            $table->date('publication_date');
            $table->string('isbn');
            $table->unsignedBigInteger('writer_id');
            $table->foreign('writer_id')
            ->references('id')
            ->on('writers')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->unsignedBigInteger('editorial_id');
            $table->foreign('editorial_id')
            ->references('id')
            ->on('editorials')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->unsignedBigInteger('library_id');
            $table->foreign('library_id')
            ->references('id')
            ->on('libraries')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('books');
    }
};
