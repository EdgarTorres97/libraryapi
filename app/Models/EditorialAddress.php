<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditorialAddress extends Model
{
    use HasFactory;
    protected $fillable = [
        'postal_code',
        'street'
    ];

    public function Editorial()
    {
        return $this->belongsTo(Editorial::class);
    }
}
