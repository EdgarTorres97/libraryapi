<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Book extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name',
        'synopsis',
        'language',
        'page_number',
        'publication_date',
        'isbn',
        'writer_id',
        'editorial_id',
        'library_id'
    ];

    public function writer() {
        return $this->belongsTo(Writer::class);
    }

    public function editorial() {
        return $this->belongsTo(Editorial::class);
    }

    public function library() {
        return $this->belongsTo(Library::class);
    }

}
