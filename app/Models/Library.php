<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'library_address_id'
    ];

    public function library_address(){
        return $this->hasOne(LibraryAddress::class, 'id','library_address_id');
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
