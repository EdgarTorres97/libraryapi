<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibraryAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'postal_code',
        'street'
    ];

    public function library()
    {
        return $this->belongsTo(Library::class);
    }
}
