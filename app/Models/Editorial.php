<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'editorial_address_id'
    ];

    public function editorial_address() {
        return $this->hasOne(EditorialAddress::class, 'id','editorial_address_id');
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
