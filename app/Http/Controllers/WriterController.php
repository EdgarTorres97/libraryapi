<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Writer;

class WriterController extends Controller
{
    public function getAll()
    {
        $writers = Writer::all();

        return response()->json($writers,200);
    }
}
