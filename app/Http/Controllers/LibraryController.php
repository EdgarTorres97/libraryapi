<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Library;

class LibraryController extends Controller
{
    public function getAll()
    {
        $libraries = Library::with('library_address')->get();

        return response()->json($libraries,200);
    }
}
