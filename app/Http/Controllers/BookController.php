<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Book;
use App\Models\Writer;
use App\Models\Editorial;
use App\Models\Library;

class BookController extends Controller
{
    private $writer;
    private $editorial;
    private $library;

    public function index()
    {
        $books = Book::withTrashed()->with('writer','editorial','library')->paginate(5);

        return response()->json($books,200);
    }

    public function find($word)
    {
        try {
            $pagination_books = Book::withTrashed()->with('writer','editorial','library')
            ->Where('name', 'like', '%' . $word . '%')
            ->orWhere('language', 'like', '%' . $word . '%')
            ->orWhere('synopsis', 'like', '%' . $word . '%')
            ->orWhere('isbn', 'like', '%' . $word . '%')
            ->orWhereHas('writer', function ($query) use($word) {
                $query->where('first_name', 'like', '%' . $word . '%');
            })
            ->orWhereHas('editorial', function ($query) use($word) {
                $query->where('name', 'like', '%' . $word . '%');
            })
            ->orWhereHas('library', function ($query) use($word) {
                $query->where('name', 'like', '%' . $word . '%');
            })
            ->paginate(5);
            if(!$pagination_books->isEmpty()){

                return response()->json($pagination_books,200);
            }else{
                return response([
                    "status" =>false,
                    "message" =>"No se ha encontrado el libro",
                ],200);
                
            }
            
        } catch (\Throwable $e) {
            return response()->json([
                "status" =>false,
                $e,
            ],422);
        }

    }

    public function store(Request $request)
    {
        try {
            $rules = [
                'name' => 'required|string|min:1|max:100',
                'synopsis' => 'required|string|min:1|max:100',
                'language' => 'required|string|min:1|max:100',
                'page_number' => 'required|numeric|min:1',
                'publication_date' => 'required',
                'isbn' => 'required|string|min:1|max:100',
                'writer_id' => [
                    'required',
                    function ($attribute, $value, $fail) use($request){
                        $this->writer = Writer::find($request->writer_id);
                        
                        if(!$this->writer){
                            $fail('No existe el escritor proporcionado');
                        }
                    },
                    'numeric',
                    'min:1'
                ],
                'editorial_id' => [
                    'required',
                    function ($attribute, $value, $fail) use($request){
                        $this->editorial = Editorial::find($request->editorial_id);
                        
                        if(!$this->editorial){
                            $fail('No existe la editorial proporcionada');
                        }
                    },
                    'numeric',
                    'min:1'
                ],
                'library_id' => [
                    'required',
                    function ($attribute, $value, $fail) use($request){
                        $this->library = Library::find($request->library_id);
                        
                        if(!$this->library){
                            $fail('No existe la libreria proporcionada');
                        }
                    },
                    'numeric',
                    'min:1'
                ]
            ];
            $validator = \Validator::make($request->input(),$rules);
            if($validator->fails()){
                return response()->json([
                    'status' => false,
                    'errors' => $validator->errors()->all()
                ],400);
            }
    
            DB::beginTransaction();

            $book = new Book;
            $book->name = $request->name;
            $book->synopsis = $request->synopsis;
            $book->language = $request->language;
            $book->page_number = $request->page_number;
            $book->publication_date = $request->publication_date;
            $book->isbn = $request->isbn;
            $book->writer_id = $request->writer_id;
            $book->editorial_id = $request->editorial_id;
            $book->library_id = $request->library_id;
            $book->save();

            DB::commit();

            return response()->json([
                "status" =>true,
                "message" =>"Se ha creado el libro con exito",
                "book" => $book
            ],200);
    

        } catch (\Throwable $e) {

            DB::rollBack();
            return response()->json([
                "status" => false,
                "message" => "Ha ocurrido un error",
            ],422);

        }
        
    }

    public function show($id)
    {
        $book = Book::findOrFail($id);
        return response()->json($book,200);
    }

    public function update(Request $request, $id)
    {
        //
        try {
            $book = Book::findOrFail($id);
            $rules = [
                'name' => 'required|string|min:1|max:100',
                'synopsis' => 'required|string|min:1|max:100',
                'language' => 'required|string|min:1|max:100',
                'page_number' => 'required|numeric',
                'publication_date' => 'required',
                'isbn' => 'required|string|min:1|max:100',
                'writer_id' => [
                    'required',
                    function ($attribute, $value, $fail) use($request){
                        $this->writer = Writer::find($request->writer_id);
                        
                        if(!$this->writer){
                            $fail('No existe el escritor proporcionado');
                        }
                    },
                    'numeric',
                    'min:1'
                ],
                'editorial_id' => [
                    'required',
                    function ($attribute, $value, $fail) use($request){
                        $this->editorial = Editorial::find($request->editorial_id);
                        
                        if(!$this->editorial){
                            $fail('No existe la editorial proporcionada');
                        }
                    },
                    'numeric',
                    'min:1'
                ],
                'library_id' => [
                    'required',
                    function ($attribute, $value, $fail) use($request){
                        $this->library = Library::find($request->library_id);
                        
                        if(!$this->library){
                            $fail('No existe la libreria proporcionada');
                        }
                    },
                    'numeric',
                    'min:1'
                ]
            ];
            $validator = \Validator::make($request->input(),$rules);
            if($validator->fails()){
                return response()->json([
                    'status' => false,
                    'errors' => $validator->errors()->all()
                ],400);
            }
            DB::beginTransaction();

            $book->name = $request->name;
            $book->synopsis = $request->synopsis;
            $book->language = $request->language;
            $book->page_number = $request->page_number;
            $book->publication_date = $request->publication_date;
            $book->isbn = $request->isbn;
            $book->writer_id = $request->writer_id;
            $book->editorial_id = $request->editorial_id;
            $book->library_id = $request->library_id;
            $book->save();

            DB::commit();

            return response()->json([
                "status" =>true,
                "message" =>"Se ha actualizado el libro con exito",
                "book" => $book
            ],200);

        } catch (\Throwable $e) {

            DB::rollBack();

            return response()->json([
                "status" => false,
                "message" => "Ha ocurrido un error",
            ],422);
        }

    }

    public function destroy($id)
    {
        //
        try {
            $book = Book::findOrFail($id);

            DB::beginTransaction();

            $book->delete();

            DB::commit();

            return response()->json([
                "status" =>true,
                "message" =>"Se ha eliminado el libro con exito"
            ],200);

        } catch (\Throwable $e) {

            DB::rollBack();

            return response()->json([
                "status" => false,
                "message" => "Ha ocurrido un error",
            ],422);

        }
    }

    public function restore($id)
    {
        $this->function_name = 'restore';
        try {
            DB::beginTransaction();
            $book = Book::withTrashed()->findOrFail($id);
                if($book){
                    $book->restore();
                    DB::commit();
                    
                    return response([
                        "status" =>true,
                        "message" =>"Se ha restaurado el libro con exito",
                    ],200);
                }else{
                    DB::rollBack();
                    return response()->json([
                        "status" =>false,
                        "message" => "No se puede restaurar el libro o no existe"
                    ],422);
                }
                


        } catch (\Throwable $e) {

            DB::rollBack();

            return response()->json([
                "status" => false,
                "message" => "Ha ocurrido un error",
            ],422);

        }
    }

    public function delete($id)
    {
        //
        try {
            $book = Book::findOrFail($id);

            DB::beginTransaction();

            $book->forceDelete();

            DB::commit();

            return response()->json([
                "status" =>true,
                "message" =>"Se ha eliminado el libro con exito"
            ],200);

        } catch (\Throwable $e) {

            DB::rollBack();

            return response()->json([
                "status" => false,
                "message" => "Ha ocurrido un error",
            ],422);

        }
    }
}
