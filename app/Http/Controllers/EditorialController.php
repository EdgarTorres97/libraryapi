<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Editorial;

class EditorialController extends Controller
{
    public function getAll()
    {
        $editorials = Editorial::with('editorial_address')->get();

        return response()->json($editorials,200);
    }
}
