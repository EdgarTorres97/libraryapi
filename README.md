
# Configuración del proyecto

```sh
composer i
npm i
```

## Configuración del proyecto

crear archivo .env en la raiz y copiar el contenido del archivo .env.example,
sustituir los valores referenets a la base de datos por los correspondientes a utilizar

```sh
DB_CONNECTION=CONEXION_BASE_DE_DATOS
DB_HOST=IP_HOST_BASE_DE_DATOS
DB_PORT=PUERTO_BASE_DE_DATOS
DB_DATABASE=NOMBRE BASE DE DATOS
DB_USERNAME=USUARIO
DB_PASSWORD=CLAVE
```
Generar key con el comando: 
```sh
php artisan key:generate
```

Ejecutar Migraciones con el comando: 
```sh
php artisan migrate --seed
```
### Compilar proyecto en desarrollo

```sh
php artisan serve
```
